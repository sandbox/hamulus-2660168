Taxonomy UI Improvements module
==================

INTRODUCTION
------------

At the moment the module provides only two options: batch removal of terms and 
batch adding terms directly from list terms page.


REQUIREMENTS
------------

Taxonomy module enabled


INSTALLATION
------------

Install as you would normally install a contributed Drupal module.


CONFIGURATION
-------------

The module has no menu or modifiable settings.


TROUBLESHOOTING
---------------

There are no known troubles with the module.
