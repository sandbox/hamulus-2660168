<?php

/**
 * @file
 * Module file for Taxonomy UI Improvements.
 *
 * @todo: API PHPdocs
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function tuii_form_taxonomy_overview_terms_alter(&$form, &$form_state, $form_id) {

  // Check for confirmation forms.
  if (isset($form_state['delete_confirmation'])) {
    $form = tuii_confirm_delete_form($form, $form_state,
        $form['#vocabulary']->vid);
  }

  if (empty($form['reset_alphabetical']) && empty($form['delete_confirmation']) &&
      empty($form['confirm_delete'])) {

    foreach ($form as $key => $element) {

      if (is_numeric(strpos($key, 'tid:'))) {

        // Adding fields to prevent warnings.
        if ($form['#total_entries'] == 1) {

          $form['#parent_fields'] = TRUE;
          $form[$key]['tid'] = array(
            '#type'  => 'hidden',
            '#value' => $form[$key]['#term']['tid'],
          );
          $form[$key]['parent'] = array(
            '#type'          => 'hidden',
            // It needs to be changeable by the javascript.
            '#default_value' => $form[$key]['#term']['parent'],
          );
          $form[$key]['depth'] = array(
            '#type'          => 'hidden',
            // The depth is modified by javascript, so it's a default_value.
            '#default_value' => $form[$key]['#term']['depth'],
          );
          $form[$key]['weight'] = array(
            '#type'          => 'weight',
            '#delta'         => 0,
            '#title_display' => 'invisible',
            '#title'         => t('Weight for added term'),
            '#default_value' => $form[$key]['#term']['weight'],
          );
        }

        $form[$key]['delete'] = array(
          '#type'  => 'checkbox',
          '#title' => t('delete'),
        );
      }
    }

    $form['bulkfs'] = array(
      '#type'        => 'fieldset',
      '#title'       => t('Bulk terms add'),
      '#weight'      => 5,
      '#collapsible' => TRUE,
      '#collapsed'   => TRUE,
    );

    $form['bulkfs']['bulkterms'] = array(
      '#type'          => 'textarea',
      '#description'   => t('Insert terms to add one per line'),
      '#default_value' => '',
    );

    // This dummy field is for preventing warning from original taxonomy module.
    $form['bulkfs']['tid'] = array(
      '#type'  => 'hidden',
      '#value' => 'dummy',
    );

    // Adding 'Save' button anyway.
    if (!empty($form['#total_entries']) && $form['#total_entries'] <= 1) {
      $form['actions'] = array('#type' => 'actions', '#tree' => FALSE);
      $form['actions']['submit'] = array(
        '#type'  => 'submit',
        '#value' => t('Save'),
      );
    }

    $form['#theme'] = 'tuii_overview_terms';
    $form['#submit'][] = 'tuii_form_taxonomy_overview_terms_alter_submit';
  }
}

/**
 * Submit handler for taxonomy overview form.
 */
function tuii_form_taxonomy_overview_terms_alter_submit($form, &$form_state) {

  $vid = $form['#vocabulary']->vid;

  if (!empty($form_state['values']['bulkfs']['bulkterms'])) {

    $new_terms = explode("\n", $form_state['values']['bulkfs']['bulkterms']);

    foreach ($new_terms as $newterm) {
      $term = new stdClass();
      $term->name = trim($newterm);
      $term->vid = $vid;
      $term->parent = array(0);

      if ($term->name) {
        taxonomy_term_save($term);
      }
    }
  }

  $terms_to_delete = array();

  foreach ($form_state['values'] as $key => $element) {
    if (is_numeric(strpos($key, 'tid:'))) {
      $tid = $element['tid'];

      if ($element['delete']) {
        $terms_to_delete[] = $tid;
      }
    }
  }

  if ($terms_to_delete) {

    // Rebuild the form to confirm the delete action.
    $form_state['rebuild'] = TRUE;
    $form_state['delete_confirmation'] = TRUE;
    $form_state['terms_to_delete'] = $terms_to_delete;
    return;
  }
}

/**
 * Implements hook_theme().
 */
function tuii_theme() {

  return array(
    'tuii_overview_terms' => array(
      'render element' => 'form',
    ),
  );
}

/**
 * Theme output.
 */
function theme_tuii_overview_terms($variables) {
  $form = $variables['form'];

  $page_entries = $form['#page_entries'];
  $back_step = $form['#back_step'];
  $forward_step = $form['#forward_step'];

  /* Add drag and drop if parent fields are present in the form. */

  if ($form['#parent_fields']) {
    drupal_add_tabledrag('taxonomy', 'match', 'parent', 'term-parent',
        'term-parent', 'term-id', FALSE);
    drupal_add_tabledrag('taxonomy', 'depth', 'group', 'term-depth', NULL, NULL,
        FALSE);
    drupal_add_js(drupal_get_path('module', 'taxonomy') . '/taxonomy.js');
    drupal_add_js(array('taxonomy' => array('backStep' => $back_step, 'forwardStep' => $forward_step)),
        'setting');
    drupal_add_css(drupal_get_path('module', 'taxonomy') . '/taxonomy.css');
  }
  drupal_add_tabledrag('taxonomy', 'order', 'sibling', 'term-weight');

  $errors = form_get_errors() != FALSE ? form_get_errors() : array();
  $rows = array();
  foreach (element_children($form) as $key) {
    if (isset($form[$key]['#term'])) {
      $term = &$form[$key];

      $row = array();

      $row[] = (isset($term['#term']['depth']) && $term['#term']['depth'] > 0 ?
              theme('indentation', array('size' => $term['#term']['depth'])) : '') .
          drupal_render($term['view']);

      if ($form['#parent_fields']) {
        $term['tid']['#attributes']['class'] = array('term-id');
        $term['parent']['#attributes']['class'] = array('term-parent');
        $term['depth']['#attributes']['class'] = array('term-depth');
        $row[0] .= drupal_render($term['parent']) . drupal_render($term['tid']) . drupal_render($term['depth']);
      }
      $term['weight']['#attributes']['class'] = array('term-weight');
      $row[] = $term['#term']['tid'];
      $row[] = drupal_render($term['weight']);
      $row[] = drupal_render($term['edit']);
      $row[] = drupal_render($term['delete']);
      $row = array('data' => $row);
      $rows[$key] = $row;
    }
  }

  // Add necessary classes to rows.
  $row_position = 0;
  foreach ($rows as $key => $row) {
    $rows[$key]['class'] = array();
    if (isset($form['#parent_fields'])) {
      $rows[$key]['class'][] = 'draggable';
    }

    // Add classes that mark which terms belong to previous and next pages.
    if ($row_position < $back_step || $row_position >= $page_entries - $forward_step) {
      $rows[$key]['class'][] = 'taxonomy-term-preview';
    }

    if ($row_position !== 0 && $row_position !== count($rows) - 1) {
      if ($row_position == $back_step - 1 || $row_position == $page_entries - $forward_step -
          1) {
        $rows[$key]['class'][] = 'taxonomy-term-divider-top';
      }
      elseif ($row_position == $back_step || $row_position == $page_entries - $forward_step) {
        $rows[$key]['class'][] = 'taxonomy-term-divider-bottom';
      }
    }

    // Add an error class if this row contains a form error.
    foreach ($errors as $error_key => $error) {
      if (strpos($error_key, $key) === 0) {
        $rows[$key]['class'][] = 'error';
      }
    }
    $row_position++;
  }

  if (empty($rows)) {
    $rows[] = array(array('data' => $form['#empty_text'], 'colspan' => '3'));
  }

  $header = array(t('Name'), t('TID'), t('Weight'), t('Edit'), t('Delete'));
  $output = theme('table',
      array(
        'header'     => $header,
        'rows'       => $rows,
        'attributes' => array('id' => 'taxonomy'),
      )
  );
  $output .= drupal_render_children($form);
  $output .= theme('pager');

  return $output;
}

/**
 * Form builder to confirm terms deleting.
 */
function tuii_confirm_delete_form($form, &$form_state, $vid) {

  // Capturing messages to prevent default message on delete form.
  drupal_get_messages();

  $vocabulary = taxonomy_vocabulary_load($vid);

  $form['type'] = array('#type' => 'value', '#value' => 'vocabulary');
  $form['vid'] = array('#type' => 'value', '#value' => $vid);
  $form['machine_name'] = array('#type' => 'value', '#value' => $vocabulary->machine_name);
  $form['name'] = array('#type' => 'value', '#value' => $vocabulary->name);
  $form['confirm_delete'] = array('#type' => 'value', '#value' => TRUE);

  $form['#submit'] = array('tuii_confirm_delete_form_submit');
  unset($form['#theme']);

  foreach ($form as $key => $form_item) {
    if (is_numeric(strpos($key, 'tid:'))) {
      unset($form[$key]);
    }
  }

  $terms_to_delete = $form_state['terms_to_delete'];

  $terms_to_delete_count = count($terms_to_delete);

  $terms_to_show = array_slice($terms_to_delete, 0, 5);

  $terms_to_show_count = count($terms_to_show);

  $terms_to_show = taxonomy_term_load_multiple($terms_to_show);

  $terms_to_show_names = array();

  foreach ($terms_to_show as $term_to_show) {
    $terms_to_show_names[] = $term_to_show->name;
  }

  $terms_to_show_names = implode(',<br>', $terms_to_show_names);

  $more_count = $terms_to_delete_count - $terms_to_show_count;

  $more_add_text = '';
  if ($more_count >= 1) {
    $more_add_text = '<br>' . t('and @count more',
            array('@count' => $more_count));
  }

  $form = confirm_form($form,
      t('Are you sure want to delete terms?',
          array('%title' => $vocabulary->name)),
      'admin/structure/taxonomy/' . $vocabulary->machine_name,
      t('Terms deleting cannot be undone.<br>Are you sure want to delete terms:') . '<br>' . $terms_to_show_names . $more_add_text . '?',
      t('Delete terms'), t('Cancel'));

  return $form;
}

/**
 * Submit handler to delete terms.
 *
 * @see tuii_confirm_delete()
 */
function tuii_confirm_delete_form_submit($form, &$form_state) {

  foreach ($form_state['terms_to_delete'] as $tid) {
    taxonomy_term_delete($tid);
  }

  $form_state['redirect'] = 'admin/structure/taxonomy/' . $form_state['values']['machine_name'];

  unset($form_state['delete_confirmation']);

  drupal_set_message(t('Terms successfully deleted'));
}
